function onLoad() {
    console.log("js loaded")
}

window.onload = function(){
    var clipboard = new Clipboard('.btn');
    clipboard.on('success', function(e) {
        console.log(e);
    });
    clipboard.on('error', function(e) {
        console.log(e);
    });
}

String.prototype.replaceAt=function(index, character) {
    return this.substr(0, index) + character + this.substr(index+character.length);
}

function findSurrogatePair(point) {
  // assumes point > 0xffff
  var offset = point - 0x10000,
      lead = 0xd800 + (offset >> 10),
      trail = 0xdc00 + (offset & 0x3ff);
  return [lead.toString(16), trail.toString(16)];
}

function isInArray(array, item) {
    return array.indexOf(item.toLowerCase()) > -1;
}

function isEmojiInArray(array, item) {
    return array.indexOf(item) > -1;
}


var ranges = [
  '\ud83c[\udf00-\udfff]', // U+1F300 to U+1F3FF
  '\ud83d[\udc00-\ude4f]', // U+1F400 to U+1F64F
  '\ud83d[\ude80-\udeff]'  // U+1F680 to U+1F6FF
];

var emojiStringToArray = function (str) {
  // make sure all emojis are split correctly (hell)
  split = str.split(/[#0-9]\u20E3|([\xA9\xAE\u203C\u2047-\u2049\u2122\u2139\u3030\u303D\u3297\u3299][\uFE00-\uFEFF]?|[\u2190-\u21FF][\uFE00-\uFEFF]?|[\u2300-\u23FF][\uFE00-\uFEFF]?|[\u2460-\u24FF][\uFE00-\uFEFF]?|[\u25A0-\u25FF][\uFE00-\uFEFF]?|[\u2600-\u27BF][\uFE00-\uFEFF]?|[\u2900-\u297F][\uFE00-\uFEFF]?|[\u2B00-\u2BF0][\uFE00-\uFEFF]?|(?:\uD83C[\uDC00-\uDFFF]|\uD83D[\uDC00-\uDEFF])[\uFE00-\uFEFF]?)/g);
  arr = [];
  for (var i=0; i<split.length; i++) {
    char = split[i]
    if (char !== "") {
        arr.push(char);
    }
  }
  return arr;
};

alphabet = [' ', '-', '!', '.', '"', '\'', '?', ',', 'a', 'e', 'i', 'o', 'u', 'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
console.log(String.fromCodePoint(0x270B));
key = [' ', String.fromCodePoint(0x270B), String.fromCodePoint(0x1F64C), String.fromCodePoint(0x270A), String.fromCodePoint(0x270C), String.fromCodePoint(0x1F44D), String.fromCodePoint(0x1F64F), String.fromCodePoint(0x1F44F), String.fromCodePoint(0x1F602), String.fromCodePoint(0x1F603), String.fromCodePoint(0x1F604), String.fromCodePoint(0x1F605), String.fromCodePoint(0x1F606), String.fromCodePoint(0x1F607), String.fromCodePoint(0x1F608), String.fromCodePoint(0x1F609), String.fromCodePoint(0x1F610), String.fromCodePoint(0x1F611), String.fromCodePoint(0x1F612), String.fromCodePoint(0x1F613), String.fromCodePoint(0x1F614), String.fromCodePoint(0x1F615), String.fromCodePoint(0x1F616), String.fromCodePoint(0x1F617), String.fromCodePoint(0x1F618), String.fromCodePoint(0x1F619), String.fromCodePoint(0x1F620), String.fromCodePoint(0x1F621), String.fromCodePoint(0x1F622), String.fromCodePoint(0x1F623), String.fromCodePoint(0x1F624), String.fromCodePoint(0x1F625), String.fromCodePoint(0x1F626), String.fromCodePoint(0x1F627), String.fromCodePoint(0x1F628), String.fromCodePoint(0x1F629), String.fromCodePoint(0x1F630), String.fromCodePoint(0x1F631), String.fromCodePoint(0x1F632), String.fromCodePoint(0x1F633), String.fromCodePoint(0x1F634), String.fromCodePoint(0x1F635), String.fromCodePoint(0x1F636), String.fromCodePoint(0x1F637), String.fromCodePoint(0x1F638)]


function convertToEmoji() {
    var mainText = document.getElementById("outputField");
    var inField = document.getElementById("convertToEmojiField").value;
    
    var processedString = inField.replace(/[^\w\s!.,"'?-]/gi, '').toLowerCase().split("")
    var newString = [];
    
    console.log(processedString)
    
    for (item in processedString) {
        newString.push(key[alphabet.indexOf(processedString[item])]);
        //console.log(String.fromCodePoint(newString[0]));
    }
    console.log(mainText.value);
    mainText.value = newString.join("");
    console.log(findSurrogatePair(0x1F44C));
    console.log(findSurrogatePair(0x270B));
}

function convertToText() {
    var mainText = document.getElementById("outputField")
    var inField = document.getElementById("convertToTextField").value;
    
    var processedString = emojiStringToArray(inField);
    var newString = [];
    
    console.log(processedString);
    //console.log(isEmojiInArray(key, processedString[0]));
    
    for (item in processedString) {
        newString.push(alphabet[key.indexOf(processedString[item])]);
        //console.log(String.fromCodePoint(newString[0]));
    }
    mainText.value = newString.join("");
}